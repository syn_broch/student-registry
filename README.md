# How to build this locally application

* run `mvn package`
* build docker file `docker build -t synpulse/student-registry:0.0.4 .`
* run container with `docker run -p 9090:9090 --env SPRING_PROFILE=dev --env CONFIG_FILE_PATH=/config/application-dev.properties -v ./config/application-dev.properties:/config/application.properites synpulse/student-registry:0.0.3`