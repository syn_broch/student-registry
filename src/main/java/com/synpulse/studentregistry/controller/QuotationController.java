package com.synpulse.studentregistry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synpulse.studentregistry.model.Quote;
import com.synpulse.studentregistry.service.QuotationService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class QuotationController {
	
	@Autowired
	QuotationService service;
	
	@RequestMapping(value = "/random-quote")
	public Quote getRandomQuote() {
		return service.getRandomQuote();
	}
}
