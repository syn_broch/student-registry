package com.synpulse.studentregistry.controller;

import com.synpulse.studentregistry.api.StudentsApi;
import com.synpulse.studentregistry.exception.IllegalUseofMultipleQueryParametersException;
import com.synpulse.studentregistry.model.Student;
import com.synpulse.studentregistry.service.RegistryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class RegistryController implements StudentsApi {

    @Autowired
    RegistryService service;

    @Override
    public ResponseEntity<List<Student>> getStudents(String country, String role) {
        if (StringUtils.isEmpty(country) && StringUtils.isEmpty(role)) {
            return new ResponseEntity<>(service.getAllStudents(), new HttpHeaders(), HttpStatus.OK);
        } else if (!StringUtils.isEmpty(country) && !StringUtils.isEmpty(role)) {
            throw new IllegalUseofMultipleQueryParametersException();
        } else if (!StringUtils.isEmpty(country)) {
            return new ResponseEntity<>(service.getStudentsByCountry(country), new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(service.getStudentsByRole(role), new HttpHeaders(), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<Student> getStudent(Integer id) {
        return new ResponseEntity<>(service.getStudentById(id), new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> addStudent(@Valid Student student) {
        service.registerStudent(student);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Void> deleteStudent(Integer id) {
        service.deleteStudent(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Void> updateStudent(Integer id, @Valid Student student) {
        service.updateStudent(id, student);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
