package com.synpulse.studentregistry.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllegalUseofMultipleQueryParametersException extends RuntimeException {
    public IllegalUseofMultipleQueryParametersException() {
        super("Only one query parameter is allowed.");
    }
}
