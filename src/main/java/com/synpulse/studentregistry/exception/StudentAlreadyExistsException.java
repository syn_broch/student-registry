package com.synpulse.studentregistry.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class StudentAlreadyExistsException extends RuntimeException {
	public StudentAlreadyExistsException(Integer id) {
		super("Student already exists : " + id);
	}
}
