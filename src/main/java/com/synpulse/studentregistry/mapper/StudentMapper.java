package com.synpulse.studentregistry.mapper;

import com.synpulse.studentregistry.model.Student;
import com.synpulse.studentregistry.model.StudentDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {
    Student dtoToStudent(StudentDTO studentDTO);

    List<Student> dtoToStudent(List<StudentDTO> list);

    StudentDTO studentToDto(Student student);

}
