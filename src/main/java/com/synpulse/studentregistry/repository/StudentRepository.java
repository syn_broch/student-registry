package com.synpulse.studentregistry.repository;

import com.synpulse.studentregistry.model.StudentDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<StudentDTO, Integer> {
    List<StudentDTO> findAllByCountry(String country);

    List<StudentDTO> findAllByRole(String role);

    Optional<StudentDTO> findOneByFirstNameAndLastNameAndCountryAndRole(String firstName, String lastName, String country, String role);
}
