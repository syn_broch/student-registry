package com.synpulse.studentregistry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.synpulse.studentregistry.model.Quote;

@Service
public class QuotationService {
	
	@Autowired
	RestTemplate restTemplate;
	
	public Quote getRandomQuote() {
		return restTemplate.getForObject("https://gturnquist-quoters.cfapps.io/api/random", Quote.class);
	}
}
