package com.synpulse.studentregistry.service;

import com.synpulse.studentregistry.exception.StudentAlreadyExistsException;
import com.synpulse.studentregistry.exception.StudentNotFoundException;
import com.synpulse.studentregistry.mapper.StudentMapper;
import com.synpulse.studentregistry.model.Student;
import com.synpulse.studentregistry.model.StudentDTO;
import com.synpulse.studentregistry.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RegistryService {
    @Autowired
    StudentRepository repository;
    @Autowired
    StudentMapper mapper;

    public List<Student> getAllStudents() {
        return mapper.dtoToStudent(repository.findAll());
    }

    public Student getStudentById(Integer id) {
        return mapper.dtoToStudent(repository.findById(id).get());
    }

    public List<Student> getStudentsByCountry(String country) {
        return mapper.dtoToStudent(repository.findAllByCountry(country));
    }

    public List<Student> getStudentsByRole(String role) {
        return mapper.dtoToStudent(repository.findAllByRole(role));
    }

    public void registerStudent(Student newStudent) {
        Optional<StudentDTO> existingStudent = repository.findOneByFirstNameAndLastNameAndCountryAndRole(newStudent.getFirstName(), newStudent.getLastName(), newStudent.getCountry(), newStudent.getRole());
        if (existingStudent.isPresent()) {
            throw new StudentAlreadyExistsException(existingStudent.get().getId());
        } else {
            repository.save(mapper.studentToDto(newStudent));
        }
    }

    public void updateStudent(Integer id, Student updatedStudent) {
        Optional<StudentDTO> student = repository.findById(id);
        if (student.isPresent()) {
            StudentDTO updatedStudentDto = student.get();
            updatedStudentDto.setCountry(updatedStudent.getCountry());
            updatedStudentDto.setCourse(updatedStudent.getCourse());
            updatedStudentDto.setLastName(updatedStudent.getLastName());
            updatedStudentDto.setFirstName(updatedStudent.getFirstName());
            updatedStudentDto.setRole(updatedStudent.getRole());
            repository.save(updatedStudentDto);

        } else {
            throw new StudentNotFoundException(id);
        }
    }

    public void deleteStudent(Integer id) {
        repository.deleteById(id);
    }
}
 