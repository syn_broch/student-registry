package com.synpulse.studentregistry;

import com.synpulse.studentregistry.mapper.StudentMapper;
import com.synpulse.studentregistry.model.Student;
import com.synpulse.studentregistry.model.StudentDTO;
import com.synpulse.studentregistry.repository.StudentRepository;
import com.synpulse.studentregistry.service.RegistryService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegistryServiceTest {
  private static final List<StudentDTO> studentsList =
          Stream.of(
                  new StudentDTO(1, "Roman", "Brauchle", "Germany", "API Bootcamp", "Teacher"),
                  new StudentDTO(1, "Roland", "Brunner", "Switzerland", "API Bootcamp", "Teacher"),
                  new StudentDTO(1, "Simon", "Alioth", "Switzerland", "API Bootcamp", "Observer"),
                  new StudentDTO(1, "Weili", "Gao", "China", "API Bootcamp", "Student"))
                  .collect(Collectors.toList());
  @Autowired
  RegistryService service;
  @Autowired
  StudentMapper mapper;
  @MockBean
  StudentRepository repository;
  ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

  @Test
  void testGetAllStudents() {
    // Mock Repository
    when(repository.findAll()).thenReturn(studentsList);
    // Invoke Service
    List<Student> students = service.getAllStudents();
    // check result
    assertEquals(4, students.size());
  }

  @Test
  void testGetStudentById() {
    when(repository.findById(any())).thenReturn(Optional.of(studentsList.get(0)));

    Student student = service.getStudentById(1);
    assertEquals("Roman", student.getFirstName());
    assertEquals("Brauchle", student.getLastName());
    assertEquals("Germany", student.getCountry());
    assertEquals("Teacher", student.getRole());
    assertEquals("API Bootcamp", student.getCourse());
  }

  @Test
  void testGetStudentsByCountry() {
    when(repository.findAllByCountry(argumentCaptor.capture()))
            .thenReturn(
                    studentsList.stream()
                            .filter(studentDTO -> studentDTO.getCountry().equalsIgnoreCase("FakeCountry"))
                            .collect(Collectors.toList()));
    List<Student> students = service.getStudentsByCountry("FakeCountry");
    assertEquals("FakeCountry", argumentCaptor.getValue());

    students = service.getStudentsByCountry("");
    assertEquals("", argumentCaptor.getValue());

    students = service.getStudentsByCountry("china");
    assertEquals("china", argumentCaptor.getValue());
  }

  @Test
  void testGetStudentsByRole() {
    when(repository.findAllByRole(argumentCaptor.capture()))
            .thenReturn(
                    studentsList.stream()
                            .filter(studentDTO -> studentDTO.getRole().equalsIgnoreCase("Student"))
                            .collect(Collectors.toList()));
    List<Student> students = service.getStudentsByRole("FakeRole");
    assertEquals("FakeRole", argumentCaptor.getValue());

    students = service.getStudentsByRole("");
    assertEquals("", argumentCaptor.getValue());

    students = service.getStudentsByRole("teacher");
    assertEquals("teacher", argumentCaptor.getValue());
  }
}
