package com.synpulse.studentregistry;

import com.synpulse.studentregistry.mapper.StudentMapper;
import com.synpulse.studentregistry.model.Student;
import com.synpulse.studentregistry.repository.StudentRepository;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(
        classes = StudentregistryApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration
public class StepsDef {
  @Autowired
  StudentRepository studentRepository;
  @Autowired
  StudentMapper mapper;
  private Student student;
  private ResponseEntity responseEntity;

  @Given("a new student")
  public void newStudent() {
    studentRepository.deleteAll();
    student = new Student();
    student.setCountry("Switzerland");
    student.setCourse("API Bootcamp");
    student.setFirstName("Roland");
    student.setLastName("Brunner");
    student.setRole("Student");
  }

  @Given("an existing student")
  public void existingStudent() {
    student = new Student();
    student.setCountry("Switzerland");
    student.setCourse("API Bootcamp");
    student.setFirstName("Roman");
    student.setLastName("Brauchle");
    student.setRole("Student");
    studentRepository.save(mapper.studentToDto(student));
  }

  @When("I post the student")
  public void postStudent() {
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
    headers.add(HttpHeaders.ACCEPT, "*/*");
    responseEntity =
            restTemplate.exchange(
                    "http://localhost:9090/student-registry/students",
                    HttpMethod.POST,
                    new HttpEntity<>(student, headers),
                    Void.class);
  }

  @Then("I get a response of type created")
  public void iGetAResponseOfTypeCreated() {
    assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
  }

  @Then("I get a response of type Unacceptable")
  public void iGetAResponseOfTypeUnacceptable() {
    assertEquals(HttpStatus.NOT_ACCEPTABLE, responseEntity.getStatusCode());
  }
}
