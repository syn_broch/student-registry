package com.synpulse.studentregistry;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.synpulse.studentregistry.mapper.StudentMapper;
import com.synpulse.studentregistry.model.Student;
import com.synpulse.studentregistry.repository.StudentRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StudentregistryApplicationTests {

    WireMockServer wireMockServer;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    StudentRepository repository;
    @Autowired
    StudentMapper mapper;

    private static Student createStudent(Integer id, String firstName, String lastName, String course, String country, String role) {
        Student student = new Student();
        student.setCountry(country);
        student.setCourse(course);
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setId(id);
        student.setRole(role);
        return student;
    }

    @BeforeAll
    public void setUp() {
        repository.deleteAll();
        repository.save(mapper.studentToDto(createStudent(1, "Roman", "Brauchle", "API Bootcamp", "Germany", "Teacher")));
        repository.save(mapper.studentToDto(createStudent(2, "Roland", "Brunner", "API Bootcamp", "Switzerland", "Teacher")));
        repository.save(mapper.studentToDto(createStudent(3, "Simon", "Alioth", "API Bootcamp", "Switzerland", "Observer")));
        repository.save(mapper.studentToDto(createStudent(4, "Weili", "Gao", "API Bootcamp", "China", "Student")));

    }

    @Test
    void testGetStudents() {
        assertEquals(4, this.restTemplate
                .getForObject("http://localhost:" + port + "/student-registry/students", List.class).size());
    }

    @Test
    void testGetStudent() {
        assertTrue(this.restTemplate
                .getForObject("http://localhost:" + port + "/student-registry/students/1", Student.class).getFirstName().equalsIgnoreCase("Roman"));
    }


    @Test
    void testRandomQuote() {
        wireMockServer = new WireMockServer(WireMockConfiguration.options().port(9999));
        WireMock.configureFor("localhost", 9999);
        wireMockServer.start();
        setUpQuotationStub();

        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:9999/student-registry/random-quote", String.class);
        assertSame(response.getStatusCode(), HttpStatus.OK);

        wireMockServer.stop();
    }


    public void setUpQuotationStub() {
        stubFor(get(urlEqualTo("/student-registry/random-quote"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBodyFile("json/quotation.json")));
    }
}
