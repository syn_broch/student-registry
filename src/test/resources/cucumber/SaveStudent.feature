Feature: Saving a Student
  Background: We are trying to store a student in our application
    Scenario: Student does not exist
      Given a new student
      When I post the student
      Then I get a response of type created
    Scenario: Student exists
      Given an existing student
      When I post the student
      Then I get a response of type Unacceptable